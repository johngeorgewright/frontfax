socket = io.connect "#{window.location.protocol}//#{window.location.host}"

socket.on 'refreshAll', ->
	window.location.reload()

socket.on 'refreshCSS', ->
	styles = document.getElementsByTagName 'link'
	stamp  = new Date().getTime()
	i      = 0

	for style in styles
		if style.getAttribute('rel') is 'stylesheet'
			href = style.href
			queryIndex = href.indexOf '?'
			if queryIndex >= 0
				if href.indexOf('stamp', queryIndex) >= 0
					style.href = href.replace /([\?&]_frontfax_stamp=)\d+/, "$1#{stamp}"
				else
					style.href += "&_frontfax_stamp=#{stamp}"
			else
				style.href += "?_frontfax_stamp=#{stamp}"

